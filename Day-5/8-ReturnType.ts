// example of ReturnType:

type TReturnType<T extends (...args: any[]) => any> = T extends (
  ...args: any[]
) => infer R
  ? R
  : never;

function add(a: number, b: number) {
  return a + b;
}

function concatText(text1: string, text2: string) {
  return text1 + text2;
}

const addReturnType: TReturnType<typeof add> = 10;
// const addReturnType2: TReturnType<typeof add> = "10";
const concatTextReturnType: TReturnType<typeof concatText> = "Hello World";
// const concatTextReturnType2: TReturnType<typeof concatText> = 10;

console.log(addReturnType);
console.log(concatTextReturnType);
