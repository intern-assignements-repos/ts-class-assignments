// example of Partial

interface IPhone {
  id: number;
  name: string;
  model: string;
  color: string;
  price: number;
  company: string;
}

type TPhone = Partial<IPhone>;

const iphone: TPhone = {
  id: 1,
  name: "iPhone 12",
  company: "Apple",
  price: 100000,
};

type TPhone2 = Partial<IPhone> & Required<Pick<IPhone, "id" | "name">>;

const samsung: TPhone2 = {
  id: 2,
  name: "Samsung Galaxy S21",
  company: "Samsung",
  price: 50000,
};

console.log(iphone);
console.log(samsung);
