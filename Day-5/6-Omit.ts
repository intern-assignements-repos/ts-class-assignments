// example of Omit:

interface IComputer {
  id: number;
  name: string;
  model?: string;
  color?: string;
  price: number;
  company: string;
  ram?: string;
  cpu?: string;
  hdd?: string;
  ssd?: string;
  gpu?: string;
}

type TLaptop = Omit<IComputer, "ram" | "cpu" | "hdd" | "ssd" | "gpu">;

const macbook: TLaptop = {
  id: 1,
  name: "Macbook Pro",
  company: "Apple",
  price: 100000,
};

console.log(macbook);
