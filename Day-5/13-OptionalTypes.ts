// example of Optional chaining on optional type property : (no error)
interface ISports {
  name: string;
  type: string;
}

interface IStudent {
  name: string;
  age: number;
  sports?: ISports;
}

const student: IStudent = {
  name: "John",
  age: 30,
};

const sportsName = student?.sports?.name;

console.log(sportsName);

// example of Optional chaining on optional type property : (error)

// interface IStudent {
//     name: string;
//     age: number;
//     sports?: {
//         name: string;
//         type: string;
//     };
// }

// const student: IStudent = {
//     name: "John",
//     age: 30,
// };

// const sportsName = student?.sports?.name;

// console.log(sportsName);
