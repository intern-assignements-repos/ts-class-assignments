// example of InstanceType:

interface IGenre {
  genre:
    | "Action"
    | "Adventure"
    | "Comedy"
    | "Crime"
    | "Drama"
    | "Fantasy"
    | "Horror"
    | "Mystery"
    | "Romance"
    | "Thriller"
    | "Western";
}

interface ISeries {
  id: number;
  name: string;
  season: number;
  episode: number;
  rating: 1 | 2 | 3 | 4 | 5;
  genre?: IGenre["genre"][];
}

type TSeries = InstanceType<typeof Series>;

class Series {
  constructor(public series: ISeries) {}

  getSeriesName() {
    return this.series.name;
  }

  getSeriesSeason() {
    return this.series.season;
  }

  getSeriesEpisode() {
    return this.series.episode;
  }

  getSeriesRating() {
    return this.series.rating;
  }

  getSeriesGenre() {
    return this.series.genre;
  }
}

const breakingBad = new Series({
  id: 1,
  name: "Breaking Bad",
  season: 5,
  episode: 62,
  rating: 5,
  genre: ["Drama", "Crime", "Thriller"],
});

const breakingBadParams: TSeries = breakingBad;

console.log(breakingBad);
