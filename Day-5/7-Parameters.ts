// example of Parameters:

interface IUserParams {
  name: string;
  age: number;
}

type TUserParams = Parameters<(user: IUserParams) => void>;

class Person {
  constructor(public name: string, public age: number) {}

  getName() {
    return this.name;
  }

  getAge() {
    return this.age;
  }
}

const person = new Person("John", 30);

const personParams: TUserParams = [person];

console.log(person);

console.log("Person Param :", personParams);
