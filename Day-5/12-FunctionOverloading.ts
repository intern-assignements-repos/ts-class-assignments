//  example of Function Overloading:

function addThings(a: number, b: number): number;

function addThings(a: string, b: string): string;

function addThings(a: string, b: string, c: number): string;

function addThings(...args: any[]): any {
  let i = 0;
  let result = "";

  while (i < args.length) {
    result += args[i];
    i++;
  }

  return result;
}

const result1 = addThings(10, 20);
const result2 = addThings("Hello", "World");
const result3 = addThings("Hello", "World", 10);

console.log(result1);
console.log(result2);
console.log(result3);
