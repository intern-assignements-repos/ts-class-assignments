// example of Required

interface IVehicle {
  name: string;
  licensePlate: string;
  color: string;
  model: string;
}

interface ICar extends IVehicle {
  tires: number;
  seats?: number;
}

interface IMotorcycle extends IVehicle {
  engine?: string;
  wheels: number;
}

type TSuv = Required<ICar>;

type TTruck = ICar & Required<IVehicle>;

type TSportsBike = Required<IMotorcycle>;

const mahindraThar: TSuv = {
  name: "Mahindra Thar",
  licensePlate: "MH-01-AB-1234",
  color: "Red",
  model: "2020",
  tires: 4,
  seats: 5,
};

const tataAce: TTruck = {
  name: "Tata Ace",
  licensePlate: "MH-01-AB-1234",
  color: "Red",
  model: "2020",
  tires: 4,
  // seats: 5,
};

console.log(mahindraThar);
console.log(tataAce);
