// example of Keyof :

interface ISong {
  id: number;
  title: string;
  artist: string;
  album: string;
  lyrics?: string;
}

type TSearchFilter = keyof ISong;

const searchSongbyArtist: TSearchFilter = "artist";
const searchSongbyAlbum: TSearchFilter = "album";
const searchSongbyTitle: TSearchFilter = "title";
const searchSongbyLyrics: TSearchFilter = "lyrics";

const song1: ISong = {
  id: 1,
  title: "In The End",
  artist: "Linkin Park",
  album: "Hybrid Theory",
};

const song2: ISong = {
  id: 2,
  title: "Numb",
  artist: "Linkin Park",
  album: "Meteora",
};

const song3: ISong = {
  id: 3,
  title: "Baby",
  artist: "Justin Bieber",
  album: "My World 2.0",
};

const song4: ISong = {
  id: 4,
  title: "Despacito",
  artist: "Luis Fonsi",
  album: "Despacito",
};

const playlist1: ISong[] = [song1, song2, song3, song4];

const searchSong = (
  playlist: ISong[],
  searchFilter: TSearchFilter,
  searchInput: string
): ISong[] | string => {
  try {
    const res: ISong[] = playlist.filter(
      (item: ISong) => item[searchFilter] === searchInput
    );

    if (res.length === 0) throw new Error("Song is not Available");

    return res;
  } catch (err: Error | any) {
    return `${err.name} : ${err.message}`;
  }
};

function playSong(playlist: ISong[], songName: string): string {
  try {
    const song: ISong = playlist.find(
      (item: ISong) => item.title === songName
    ) as ISong;

    if (!song) throw new Error("Song is not Available on the Playlist");

    return `Playing ${song.title} by ${song.artist}, from ${song.album}`;
  } catch (err: Error | any) {
    return `${err.name} : ${err.message}`;
  }
}

console.log(playSong(playlist1, "Baby"));

console.log(searchSong(playlist1, "title", "Baby"));
