// example of Readonly

interface IAnimal {
  name: string;
  age: number;
  eat(): void;

  readonly breed: string;
}

interface IDog extends IAnimal {
  name: string;
}

const dog: IDog = {
  name: "dog",
  age: 2,
  eat() {
    console.log("dog is eating");
  },
  breed: "labrador",
};
