// example Pick :

interface Todo {
  title: string;
  description: string;
  completed: boolean;
}

type TodoPreview = Pick<Todo, "title" | "completed">;

type TodoPreview2 = Pick<Todo, "title" | "description">;

const todo: TodoPreview = {
  title: "Clean room",
  completed: false,
};

const todo2: TodoPreview2 = {
  title: "Clean room",
  description: "Clean room description",
};

console.log(todo);
console.log(todo2);
