// example ConstructorParameters :

interface IGenre {
  genre:
    | "Action"
    | "Adventure"
    | "Comedy"
    | "Crime"
    | "Drama"
    | "Fantasy"
    | "Horror"
    | "Mystery"
    | "Romance"
    | "Thriller"
    | "Western";
}

interface IConstructorParams {
  movieName: string;
  movieYear: number;
  movieRating?: 1 | 2 | 3 | 4 | 5;
  genre?: IGenre["genre"][];
}

type TConstructorParams = ConstructorParameters<
  new (movie: IConstructorParams) => Movie
>;

class Movie {
  constructor(public movie: IConstructorParams) {}

  getMovieName() {
    return this.movie.movieName;
  }

  getMovieYear() {
    return this.movie.movieYear;
  }

  getMovieRating() {
    return this.movie.movieRating;
  }

  getMovieGenre() {
    return this.movie.genre;
  }
}

const titanic = new Movie({
  movieName: "Titanic",
  movieYear: 1997,
  movieRating: 5,
  genre: ["Drama", "Romance"],
});

const movieParams: TConstructorParams = [titanic.movie];

console.log(titanic);
