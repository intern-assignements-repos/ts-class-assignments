// example Conditional Types
interface IAnimal {
  name: string;
  age: number;

  eat(): void;
}

interface IDog extends IAnimal {
  breed: string;
}

interface ICat extends IAnimal {
  color: string;
}

type TAnimal1 = IDog extends IAnimal ? string : number;

type TAnimal2 = ICat extends IAnimal ? string : number;

const animal1: TAnimal1 = "dog";
const animal2: TAnimal2 = "cat";

console.log(animal1);
console.log(animal2);
