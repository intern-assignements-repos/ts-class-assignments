const inputName = document.querySelector('#name') as HTMLInputElement;
const inputSurname = document.querySelector('#surname') as HTMLInputElement;
const submitBtn = document.getElementById("submit") as HTMLButtonElement;
const output = document.getElementById("output") as HTMLHeadingElement;


interface IPerson {
    name: string;
    surName: string
}

const printName = async (data: IPerson) => {
    output.innerHTML += (data.name + " " + data.surName);    
};

const printHello = (e: Event) => {
    e.preventDefault();
    const data: IPerson = {
        name: inputName.value,
        surName: inputSurname.value
    }    
    printName(data);
    return false;
}