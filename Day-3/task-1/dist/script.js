"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const inputName = document.querySelector('#name');
const inputSurname = document.querySelector('#surname');
const submitBtn = document.getElementById("submit");
const output = document.getElementById("output");
const printName = (data) => __awaiter(void 0, void 0, void 0, function* () {
    output.innerHTML += (data.name + " " + data.surName);
});
const printHello = (e) => {
    e.preventDefault();
    const data = {
        name: inputName.value,
        surName: inputSurname.value
    };
    printName(data);
    return false;
};
