// The program will take color names as input and output a two digit number.
// The band colors are encoded as follows:

//     Black: 0
//     Brown: 1
//     Red: 2
//     Orange: 3
//     Yellow: 4
//     Green: 5
//     Blue: 6
//     Violet: 7
//     Grey: 8
//     White: 9

// From the example above: brown-green should return 15 brown-green-violet should return 15 too, ignoring the third color.

const colorBands = {
  black: "0",
  brown: "1",
  red: "2",
  orange: "3",
  yellow: "4",
  green: "5",
  blue: "6",
  violet: "7",
  grey: "8",
  white: "9",
};

const getColorNumberFunc = (colorName: string) => {
  const colorNameList: Array<string> = colorName.split("-");

  let result: string = "";
  let i: number = 0;
  for (const item of colorNameList) {
    if (colorBands[item.toLowerCase()] != undefined) {
      if (i < 2) result += colorBands[item.toLowerCase()];
      else break;
    } else {
      return `${item} - Color is Not Defined`;
    }

    i++;
  }

  return result;
};

const readline = require("readline").createInterface({
  input: process.stdin,
  output: process.stdout,
});

readline.question(`Write the Color Band Name? => `, (colorName: string) => {
  console.log(getColorNumberFunc(colorName));
  readline.close();
});
