var initialBalance = 2000;
var addMoney = function (amount) {
    initialBalance += amount;
    return initialBalance;
};
var removeMoney = function (amount) {
    initialBalance -= amount;
    return initialBalance;
};
console.log(addMoney(5000));
console.log(removeMoney(1000));
