// The program will take color names as input and output a two digit number.
// The band colors are encoded as follows:
//     Black: 0
//     Brown: 1
//     Red: 2
//     Orange: 3
//     Yellow: 4
//     Green: 5
//     Blue: 6
//     Violet: 7
//     Grey: 8
//     White: 9
// From the example above: brown-green should return 15 brown-green-violet should return 15 too, ignoring the third color.
var colorBands = {
    black: "0",
    brown: "1",
    red: "2",
    orange: "3",
    yellow: "4",
    green: "5",
    blue: "6",
    violet: "7",
    grey: "8",
    white: "9"
};
var getColorNumberFunc = function (colorName) {
    var colorNameList = colorName.split("-");
    var result = "";
    var i = 0;
    for (var _i = 0, colorNameList_1 = colorNameList; _i < colorNameList_1.length; _i++) {
        var item = colorNameList_1[_i];
        if (colorBands[item.toLowerCase()] != undefined) {
            if (i < 2)
                result += colorBands[item.toLowerCase()];
            else
                break;
        }
        else {
            return "".concat(item, " - Color is Not Defined");
        }
        i++;
    }
    return result;
};
var readline = require("readline").createInterface({
    input: process.stdin,
    output: process.stdout
});
readline.question("Write the Color Band Name? => ", function (colorName) {
    console.log(getColorNumberFunc(colorName));
    readline.close();
});
