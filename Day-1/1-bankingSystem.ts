let initialBalance: number = 2000;

const addMoney = (amount: number) => {
    initialBalance += amount;
    return initialBalance;
}

const removeMoney = (amount: number) => {
    initialBalance -= amount;
    return initialBalance;
}

console.log(addMoney(5000));
console.log(removeMoney(1000));