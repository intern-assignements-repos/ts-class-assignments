// number type
let age: number = 22;

// string type
const day: string = "Wednesday";

// boolean type
const single: boolean = true;

// object type
const user: {
    name: string,
    username: string,
    single: boolean
} = {
    name: "Sumon",
    username: "sumonroy",
    single: true
}

user.name = "Sumon Rayy"

// array type
const students: string[] = [
    "Akshat",
    "Parminder",
    "Abhishek",
    "Rohit",
    "Ritu"
]

// tupple type
let employee: [string, number, string] = ["Charan", 12212, "Software Engineer"];

// enum type
enum Status {
    Notfound = 404,
    Ok = 200,
    Forbidden = 401,
    BadRequest = 403,
    ServerError = 500,
}

const currentStatus: Status = Status.Notfound;

console.log(`The current status of my server is ${currentStatus}`)

// void example
function printMyMessage(message: string): void{
    console.log(`My message: ${message}`);
}

console.log(printMyMessage("Have a nice day!"));

// never type
function printError(error: string): never {
    throw new Error(error);
}

// union example
let citizen: {
    name: string,
    aadhar: string,
    accountName: string,
    accountNumber: string,
    bankName: string,
    cardNo: number,
} | {
    cardNo: string,
    cvv: number,
    cardIssuer: string
} = {
    name: "Karan",
    aadhar: "24334343434",
    accountName: "Karan Gupta",
    accountNumber: "34343434",
    bankName: "SBI",
    cardNo: "343434343434343",
    cvv: 343,
    cardIssuer: "VISA"
}

// string literal
let weekDay: "sunday" | "monday" | "tuesday" = "sunday"
weekDay = "tuesday"

console.log(citizen);