// number type
var age = 22;
// string type
var day = "Wednesday";
// boolean type
var single = true;
// object type
var user = {
    name: "Sumon",
    username: "sumonroy",
    single: true
};
user.name = "Sumon Rayy";
// array type
var students = [
    "Akshat",
    "Parminder",
    "Abhishek",
    "Rohit",
    "Ritu"
];
// tupple type
var employee = ["Charan", 12212, "Software Engineer"];
// enum type
var Status;
(function (Status) {
    Status[Status["Notfound"] = 404] = "Notfound";
    Status[Status["Ok"] = 200] = "Ok";
    Status[Status["Forbidden"] = 401] = "Forbidden";
    Status[Status["BadRequest"] = 403] = "BadRequest";
    Status[Status["ServerError"] = 500] = "ServerError";
})(Status || (Status = {}));
var currentStatus = Status.Notfound;
console.log("The current status of my server is ".concat(currentStatus));
// void example
function printMyMessage(message) {
    console.log("My message: ".concat(message));
}
console.log(printMyMessage("Have a nice day!"));
// never type
function printError(error) {
    throw new Error(error);
}
// union example
var citizen = {
    name: "Karan",
    aadhar: "24334343434",
    accountName: "Karan Gupta",
    accountNumber: "34343434",
    bankName: "SBI",
    cardNo: "343434343434343",
    cvv: 343,
    cardIssuer: "VISA"
};
// string literal
var weekDay = "sunday";
weekDay = "tuesday";
console.log(citizen);
