// Write a program to demonstrate all the data types in Typescript.
// Eg. number,string,boolean,object,array,tupple,enum,union types.

const api: string = "https://fakestoreapi.com/products/1";


const fetchApi = async (url: string) => {
    try{
        const response: any = await fetch(url, {
            method: 'GET',
            headers: {
                'content-type':'application/json; charset=utf-8',
            },            
        });

        const data: {
            id: number,
            title: string,
            price: number
        } | {
            description: string,
            category: string,
            image: string
        } | {
            rating: {
                rate: number,
                count: number
            }
        } = await response.json()

        console.log(data);
    }catch(err: unknown){
        console.error(err);
    }
}

fetchApi(api);