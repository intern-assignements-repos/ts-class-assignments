// 1. What are template literals and write a type that make a template union with
// numbers and css units (em, rem, px)

// Ans : Template literals are string literals allowing embedded expressions.
// We can use multi-line strings and string interpolation features with them.
// They were called "template strings" in prior editions of the ES2015 specification.

const templateLiteral = (value: number, unit: string) => {
  return `${value}${unit}`;
};

interface ICssUnits {
  unit: "em" | "rem" | "px";
}

interface INumbers {
  value: number;
}

type TemplateUnion = INumbers & ICssUnits;

const templateUnion = (obj: TemplateUnion) => {
  try {
    if (obj.unit !== "em" && obj.unit !== "rem" && obj.unit !== "px") {
      throw new Error("Invalid Unit");
    }

    return templateLiteral(obj.value, obj.unit);
  } catch (e) {
    console.log(e);
  }
};

console.log(templateUnion({ value: 2, unit: "px" }));
