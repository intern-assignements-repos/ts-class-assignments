// 2.What are Discriminated unions and how can we make a type for
//  different status codes such that
// objects with 200 codes contain message strings
// objects with 300 codes contain warning mesage strings
// objects with 400, 500 codes contain error message string"

// Ans : Discriminated unions are a way to combine types together
// to create a single type that has a common property.
// This common property is called a discriminator.
// The discriminator is a property that is common to all the types in the union.
// The value of the discriminator property is used to determine the type of the object at runtime.

interface IResponse {
  status: number;
}

type TResponse =
  | { status: 200; message: "OK" | "Created" | "Accepted"; body: string }
  | { status: 300; message: "Multiple Choices" | "Moved Permanently" | "Found" }
  | {
      status: 400;
      message:
        | "Bad Request"
        | "Unauthorized"
        | "Payment Required"
        | "I'm a teapot";
    }
  | {
      status: 500;
      message:
        | "Internal Server Error"
        | "Not Implemented"
        | "Bad Gateway"
        | "Service Unavailable";
    };

const response = (obj: TResponse) => {
  try {
    // check for valid status code and message
    if (obj.status === 200) {
      // check for status code 200
      if (
        obj.message !== "OK" &&
        obj.message !== "Created" &&
        obj.message !== "Accepted"
      ) {
        throw new Error("Invalid Message"); // throw error if message is invalid for status code 200
      }
      console.log(obj.message); // log message
      console.log(obj.body); // log body
    } else if (obj.status === 300 || obj.status === 400 || obj.status === 500) {
      // check for status code 300, 400, 500
      if (
        obj.message !== "Multiple Choices" &&
        obj.message !== "Moved Permanently" &&
        obj.message !== "Found" &&
        obj.message !== "Bad Request" &&
        obj.message !== "Unauthorized" &&
        obj.message !== "Payment Required" &&
        obj.message !== "I'm a teapot" &&
        obj.message !== "Internal Server Error" &&
        obj.message !== "Not Implemented" &&
        obj.message !== "Bad Gateway" &&
        obj.message !== "Service Unavailable"
      ) {
        throw new Error("Invalid Message"); // throw error if message is invalid for status code 300, 400, 500
      }
      console.log(obj.message); // log message
    } else {
      throw new Error("Invalid Status Code"); // throw error if status code is invalid
    }
    return obj;
  } catch (e) {
    console.error(e); // log error
  }
};

// test cases : for 200
console.log(
  response({
    status: 200,
    message: "OK",
    body: "Hello World",
  })
);

// test cases : for 300
console.log(
  response({
    status: 300,
    message: "Multiple Choices",
  })
);

// test cases : for 400
console.log(
  response({
    status: 400,
    message: "Bad Request",
  })
);

// test cases : for 500
console.log(
  response({
    status: 500,
    message: "Internal Server Error",
  })
);

// test cases : for invalid status code
// console.log(
//   response({
//     status: 600,
//     message: "Invalid Status Code",
//   })
// );

// test cases : for invalid message
// console.log(
//   response({
//     status: 200,
//     message: "Invalid Message",
//   })
// );
